.PHONY: all
all: build-html build-pdf
	@echo "Building all"

.PHONY: archive
archive:
	@git archive -o "build/es-deposit-limit-docs-$$(git rev-parse --short HEAD).zip" HEAD

.PHONY: clean
clean:
	rm -rf build/*

build/README.html: README.adoc
	cp *.png build/
	asciidoctor -o $@ $<

build/README.pdf: README.adoc
	asciidoctor-pdf -o $@ $<

.PHONY: build-html build-pdf
build-html: build/README.html
	@echo "Building HTML"

build-pdf: build/README.pdf
	@echo "Building PDF"

